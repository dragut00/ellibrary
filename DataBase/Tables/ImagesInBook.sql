USE ellibrary_db
GO

CREATE TABLE ImagesInBook
(
  [BookID]       INT NOT NULL,
  [ImageId] INT NOT NULL,

  PRIMARY KEY CLUSTERED ([BookID] ASC, [ImageId] ASC),
  FOREIGN KEY ([BookID]) REFERENCES [Book]([BookID]),
  FOREIGN KEY ([ImageId]) REFERENCES [Image]([ImageId])
);
GO