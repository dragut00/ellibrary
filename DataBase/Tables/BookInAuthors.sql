USE ellibrary_db
GO

CREATE TABLE BookInAuthors
(
  [BookID]       INT NOT NULL,
  [AuthorID] INT NOT NULL,

  PRIMARY KEY CLUSTERED ([BookID] ASC, [AuthorID] ASC),
  FOREIGN KEY ([BookID]) REFERENCES [Book]([BookID]),
  FOREIGN KEY ([AuthorID]) REFERENCES [Author]([AuthorID])
);
GO