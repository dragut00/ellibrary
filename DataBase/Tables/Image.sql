USE ellibrary_db
GO

CREATE TABLE [Image]
(
	[ImageId] INT IDENTITY(1, 1) NOT NULL,
    [Data] VARBINARY(MAX) NOT NULL,
    [Extension] NVARCHAR(100) NOT NULL,

    PRIMARY KEY CLUSTERED ([ImageId] ASC)
);
GO