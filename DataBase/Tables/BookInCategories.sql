USE ellibrary_db
GO

CREATE TABLE BookInCategories
(
  [BookID]       INT NOT NULL,
  [CategoryID] INT NOT NULL,

  PRIMARY KEY CLUSTERED ([BookID] ASC, [CategoryId] ASC),
  FOREIGN KEY ([BookID]) REFERENCES [Book]([BookID]),
  FOREIGN KEY ([CategoryId]) REFERENCES [Category]([CategoryId])
);
GO