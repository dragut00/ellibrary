USE ellibrary_db
GO

CREATE TABLE Publishing
(
PublID INT IDENTITY(1,1) NOT NULL,
Name NVARCHAR(50) NOT NULL,
Adress NVARCHAR(50) NOT NULL,

PRIMARY KEY CLUSTERED ([PublID] ASC)
);
GO