USE ellibrary_db
GO

INSERT INTO Book(Name,YearOfPublishing,Lang,PublID,Link)
VALUES
('Book1',2000,'EN',1,'Link1'),
('Book2',2001,'EN',4,'Link2'),
('Book3',2000,'EN',2,'Link3'),
('Book4',2010,'RU',1,'Link4'),
('Book5',2009,'EN',1,'Link5'),
('Book6',2018,'EN',3,'Link6'),
('Book7',1999,'RU',5,'Link7'),
('Book8',2005,'EN',5,'Link8')
GO