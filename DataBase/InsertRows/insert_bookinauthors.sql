USE ellibrary_db
GO

INSERT INTO BookInAuthors(BookID,AuthorID)
VALUES
(1,6),
(1,3),
(2,1),
(3,2),
(3,3),
(3,5),
(4,1),
(5,2),
(5,6),
(6,6),
(7,2),
(7,4),
(8,5)
GO