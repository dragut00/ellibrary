USE ellibrary_db
GO

INSERT INTO Category(Name,ParentID)
VALUES
('Subcategory1-1',1),
('Subcategory1-2',1),
('Subcategory2-1',2),
('Subcategory3-1',3),
('Subcategory3-2',3),
('Subcategory4-1',4),
('Subcategory5-1',5),
('Subcategory5-2',5),
('Subcategory5-3',5)
GO