USE ellibrary_db
GO

CREATE PROCEDURE sp_select_author_by_ID
@authorID INT
AS
(
SELECT * FROM Author WHERE AuthorID=@authorID
);
GO