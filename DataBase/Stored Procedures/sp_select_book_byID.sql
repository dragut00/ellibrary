USE ellibrary_db
GO

CREATE PROCEDURE sp_select_book_byID
@bookID INT
AS
(
SELECT [Book].[BookID], [Book].[Name],[Book].[YearOfPublishing], [Book].[Lang], 
[Book].[PublID], [Book].[Link]
FROM [Book]
WHERE BookID=@bookID
); 
GO