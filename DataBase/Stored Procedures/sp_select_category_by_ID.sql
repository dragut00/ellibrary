USE ellibrary_db
GO

CREATE PROCEDURE sp_select_category_by_ID
@categoryID INT
AS
(
SELECT * FROM Category WHERE CategoryID=@categoryID OR ParentID=@categoryID
);
GO