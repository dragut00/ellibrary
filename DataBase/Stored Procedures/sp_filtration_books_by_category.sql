USE ellibrary_db
GO

CREATE PROCEDURE sp_filtration_books_by_category
@CategoryId INT
AS
(
SELECT [Book].[BookID], [Book].[Name]
FROM [Book]
INNER JOIN [BookInCategories]
ON [BookInCategories].[BookID]=[Book].[BookID]
INNER JOIN Category
ON Category.CategoryID=BookInCategories.CategoryID
WHERE Category.ParentID=@CategoryId OR Category.CategoryID=@CategoryId
); 
GO