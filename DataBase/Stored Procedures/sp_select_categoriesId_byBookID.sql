USE ellibrary_db
GO

CREATE PROCEDURE sp_select_categoriesId_byBookID
@bookID INT
AS
(
SELECT CategoryID FROM BookInCategories WHERE BookID=@bookID
);
GO