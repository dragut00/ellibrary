USE ellibrary_db
GO

CREATE PROCEDURE sp_select_categories
AS
(
SELECT * FROM Category WHERE ParentID IS NULL
); 
GO