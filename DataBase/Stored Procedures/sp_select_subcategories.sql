USE ellibrary_db
GO

CREATE PROCEDURE sp_select_subcategories
@parentID INT
AS

(SELECT * FROM Category WHERE ParentID=@parentID);
GO