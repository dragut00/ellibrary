USE ellibrary_db
GO

CREATE PROCEDURE sp_select_imagesId_byBookID
@bookID INT
AS
(
SELECT ImageId FROM ImagesInBook WHERE BookID=@bookID
);
GO