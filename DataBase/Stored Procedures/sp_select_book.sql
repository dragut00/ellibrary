USE ellibrary_db
GO

CREATE PROCEDURE sp_select_book
AS
(
SELECT [Book].[BookID], [Book].[Name]
FROM [Book]
); 
GO