USE ellibrary_db
GO

CREATE PROCEDURE sp_select_image_by_ID
@imageID INT
AS
(
SELECT * FROM [Image] WHERE ImageId=@imageID
);
GO