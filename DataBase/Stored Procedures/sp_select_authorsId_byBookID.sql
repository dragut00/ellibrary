USE ellibrary_db
GO

CREATE PROCEDURE sp_select_authorsId_byBookID
@bookID INT
AS
(
SELECT AuthorID FROM BookInAuthors WHERE BookID=@bookID
);
GO