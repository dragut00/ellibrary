﻿using Ellibrary.App.Business.Interfaces;
using Ellibrary.App.Data;
using Ellibrary.App.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellibrary.App.Business.Realisation
{
    public class CategoryRepository : ICategoryRepository
    {
        private ICollection<Category> _source = new List<Category>();
        IDataCategoryRepository dataCategoryRepository;

        public CategoryRepository(IDataCategoryRepository dataCategoryRepository)
        {
            if (dataCategoryRepository == null)
            {
                throw new NullReferenceException();
            }
            this.dataCategoryRepository = dataCategoryRepository;
        }

        public ICollection<Category> GetCategories()
        {
            return dataCategoryRepository.GetCategoriesFromDB();
        }

        public ICollection<Category> Items
        {
            get { return _source.ToList(); }
            set { throw new NotImplementedException(); }
        }


        public ICollection<Category> GetSubcategories(int categoryID)
        {
            return dataCategoryRepository.GetSubcategoriesFromDB(categoryID);
        }
    }
}
