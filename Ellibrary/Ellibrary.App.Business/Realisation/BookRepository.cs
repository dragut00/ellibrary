﻿using Ellibrary.App.Business.Interfaces;
using Ellibrary.App.Data;
using Ellibrary.App.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace Ellibrary.App.Business.Realisation
{
    public class BookRepository : IBookRepository
    {
        private ICollection<Book> _source = new List<Book>();
        IDataBookRepository dataBookRepository;

        public BookRepository(IDataBookRepository dataBookRepository)
        {
            if (dataBookRepository == null)
            {
                throw new NullReferenceException();
            }
            this.dataBookRepository = dataBookRepository;
            _source = dataBookRepository.GetBooksFromDB();
        }

        public ICollection<Book> Items
        {
            get { return _source; }
            set { throw new Exception(); }
        }

        public Book GetBookByID(int bookID)
        {
            return dataBookRepository.GetBookByIDFromDB(bookID);
        }

        public ICollection<Book> GetBooksByCategory(int categoryID)
        {
            return dataBookRepository.GetBooksByCategoryFromDB(categoryID);
        }

    }
}
