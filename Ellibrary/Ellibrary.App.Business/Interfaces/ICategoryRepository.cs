﻿using Ellibrary.App.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Business.Interfaces
{
    public interface ICategoryRepository:IEntityRepository<Category>
    {
        ICollection<Category> GetCategories();

        ICollection<Category> GetSubcategories(int categoryID);
    }
}
