﻿using Ellibrary.App.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Business.Interfaces
{
   public interface IBookRepository : IEntityRepository<Book>
    {
        ICollection<Book> GetBooksByCategory(int categoryID);

        Book GetBookByID(int bookID);

    }
}
