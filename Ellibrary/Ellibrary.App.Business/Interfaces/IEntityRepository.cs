﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Business.Interfaces
{
    public interface IEntityRepository<T>
    {
        ICollection<T> Items { get; set; }
    }
}
