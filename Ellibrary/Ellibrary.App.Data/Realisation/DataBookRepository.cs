﻿using Ellibrary.App.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Data.Realisation
{
    public class DataBookRepository : IDataBookRepository
    {
        public Book GetBookByIDFromDB(int bookId)
        {
            Book book=null;
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_select_book_byID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@bookID", bookId);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    book = new Book() { BookID = (int)dr[0], Name = dr[1].ToString(), YearOfPublishing = (int)dr[2], Lang = dr[3].ToString(),PublID=(int)dr[4], Link = dr[5].ToString() };
                }               
            }
            book = SetBookPublishingProp(book);
            book = SetBookAuthors(book);
            book = SetBookCategories(book);
            book = SetBookImages(book);
            return book;
        }

         private Book SetBookPublishingProp(Book book)
        {
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_select_publisihing_byPublID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@publID", book.PublID);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                Publishing publToBook = new Publishing();
                if (dr.Read())
                {
                    publToBook.PublID = (int)dr[0];
                    publToBook.Name = dr[1].ToString();
                    publToBook.Adress = dr[2].ToString();
                }
                book.Publishing = publToBook;
            }
            return book;
        }

        private Book SetBookAuthors(Book book)
        {
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;

                SqlCommand cmd1 = new SqlCommand("sp_select_authorsId_byBookID", con);
                SqlCommand cmd2;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@bookID", book.BookID);
                              

                con.Open();
                SqlDataReader dr = cmd1.ExecuteReader();
               List<int> massOfAuthorsID = new List<int>();
                while (dr.Read())
                {
                    massOfAuthorsID.Add((int)dr[0]);
                }
                dr.Close();
                ICollection<Author> authors = new List<Author>();
                for (int i = 0; i< massOfAuthorsID.Count; i++)
                {
                    cmd2  = new SqlCommand("sp_select_author_by_ID", con);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.AddWithValue("@authorID", massOfAuthorsID[i]);
                    dr = cmd2.ExecuteReader();
                    if (dr.Read())
                    {
                        authors.Add(new Author { AuthorID=(int)dr[0], FIO=dr[1].ToString(),E_mail=dr[2].ToString() });
                    }
                    dr.Close();
                }
                book.Author = authors;
            }
            return book;
        }

        private Book SetBookCategories(Book book)
        {
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;

                SqlCommand cmd1 = new SqlCommand("sp_select_categoriesId_byBookID", con);
                SqlCommand cmd2;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@bookID", book.BookID);


                con.Open();
                SqlDataReader dr = cmd1.ExecuteReader();
                List<int> massOfCategoriesID = new List<int>();
                while (dr.Read())
                {
                    massOfCategoriesID.Add((int)dr[0]);
                }
                dr.Close();
                ICollection<Category> categories = new List<Category>();
                for (int i = 0; i < massOfCategoriesID.Count; i++)
                {
                    cmd2 = new SqlCommand("sp_select_category_by_ID", con);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.AddWithValue("@categoryID", massOfCategoriesID[i]);
                    dr = cmd2.ExecuteReader();
                    if (dr.Read())
                    {
                        categories.Add(new Category { CategoryID = (int)dr[0], Name = dr[1].ToString(), ParentID = (int)dr[2] });
                    }
                    dr.Close();
                }
                book.Category = categories;
            }
            return book;
        }

        private Book SetBookImages(Book book)
        {
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;

                SqlCommand cmd1 = new SqlCommand("sp_select_imagesId_byBookID", con);
                SqlCommand cmd2;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@bookID", book.BookID);


                con.Open();
                SqlDataReader dr = cmd1.ExecuteReader();
                List<int> massOfImagesID = new List<int>();
                while (dr.Read())
                {
                    massOfImagesID.Add((int)dr[0]);
                }
                dr.Close();
                ICollection<Image> images = new List<Image>();
                for (int i = 0; i < massOfImagesID.Count; i++)
                {
                    cmd2 = new SqlCommand("sp_select_image_by_ID", con);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.AddWithValue("@imageID", massOfImagesID[i]);
                    dr = cmd2.ExecuteReader();
                    if (dr.Read())
                    {
                        images.Add(new Image { ImageId = (int)dr[0], Data = (byte[])dr[1], Extension = dr[2].ToString() });
                    }
                    dr.Close();
                }
                book.Image = images;
            }
            return book;
        }


        public ICollection<Book> GetBooksByCategoryFromDB(int categoryId)
        {
            ICollection<Book> books = new List<Book>();
            DataTable dt = new DataTable();
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_filtration_books_by_category", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryID", categoryId);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Book book = new Book() { BookID = (int)dr[0], Name = dr[1].ToString() };
                    book = SetBookImages(book);
                    books.Add(book);
                }
            }
            return books;
        }

        public ICollection<Book> GetBooksFromDB()
        {
            ICollection<Book> source=new List<Book>();
            DataTable dt = new DataTable();
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_select_book", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Book book = new Book() { BookID = (int)dr[0], Name = dr[1].ToString()};
                    book = SetBookImages(book);
                    source.Add(book);
                }
                return source;
            }
        }
    }
}
