﻿using Ellibrary.App.Data.Interfaces;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Ellibrary.App.Data.Realisation
{
    public class DataCategoryRepository : IDataCategoryRepository
    {
        public ICollection<Category> GetCategoriesFromDB()
        {
            ICollection<Category> source = new List<Category>();
            DataTable dt = new DataTable();
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_select_categories", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    source.Add(new Category() { CategoryID = (int)dr[0], Name = dr[1].ToString() });
                }
            }
            return source;
        }

        public ICollection<Category> GetSubcategoriesFromDB(int categoryId)
        {
            ICollection<Category> source = new List<Category>();
            DataTable dt = new DataTable();
            using (var con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                SqlCommand cmd = new SqlCommand("sp_select_subcategories", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@parentID", categoryId);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    source.Add(new Category() { CategoryID = (int)dr[0], Name = dr[1].ToString(), ParentID = (int)dr[2] });
                }
            }
            return source;
        }
    }
}
