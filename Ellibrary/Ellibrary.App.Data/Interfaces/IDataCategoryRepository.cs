﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Data.Interfaces
{
    public interface IDataCategoryRepository
    {
        ICollection<Category> GetCategoriesFromDB();

        ICollection<Category> GetSubcategoriesFromDB(int categoryId);
    }
}
