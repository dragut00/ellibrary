﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.Data.Interfaces
{
   public interface IDataBookRepository
    {
        ICollection<Book> GetBooksFromDB();

       Book GetBookByIDFromDB(int bookId);

        ICollection<Book> GetBooksByCategoryFromDB(int categoryId);
    }
}
