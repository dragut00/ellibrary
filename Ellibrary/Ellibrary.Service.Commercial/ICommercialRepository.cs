﻿using Ellibrary.Service.Commercial.Entities;
using Ellibrary.Service.Commercial.FaultDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Ellibrary.Service.Commercial
{
    [ServiceContract]
    public interface ICommercialRepository
    {
        [OperationContract]
        [FaultContract(typeof(CommercialNotFound))]
        Commerc[] GetCommercials();
    }
}
