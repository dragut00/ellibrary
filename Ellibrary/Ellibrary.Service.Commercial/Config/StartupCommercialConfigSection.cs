﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Ellibrary.Service.Commercial.Config
{
    public class StartupCommercialConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("pathways")]
        public PathCollection Pathways
        {
            get { return ((PathCollection)(base["pathways"])); }
        }
    }
}