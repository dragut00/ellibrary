﻿using Ellibrary.Service.Commercial.Config;
using Ellibrary.Service.Commercial.Entities;
using Ellibrary.Service.Commercial.FaultDetails;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;

namespace Ellibrary.Service.Commercial
{
    public class CommercialRepository : ICommercialRepository
    {
        private IDictionary<string, string> _pathways;
        private const int COMMERCIAL_COUNT = 3;

        public CommercialRepository()
        {
            _pathways = new Dictionary<string, string>();
            DirectoryInfo directory = null;

            try
            {
               directory = new DirectoryInfo((new CommercialPathElement()).Path = @"C:\Users\Mikita_Drahuts\Documents\LibraryProject\ellibrary\Ellibrary\Ellibrary.Service.Commercial\Images");
            }
            catch
            {
                CommercialNotFound faultDetails = new CommercialNotFound
                {
                    Message = "Commercials folder not found. Please, check path to image."
                };
                throw new FaultException<CommercialNotFound>(faultDetails);
            }



            FileInfo[] files = directory.GetFiles();
            foreach (var fileInfo in files)
            {
             _pathways.Add(fileInfo.FullName, fileInfo.Extension);
            }           
        }

        public Commerc[] GetCommercials()
        {
            var resultCollection = new List<Commerc>();
            Random random = new Random();
            for (int i = 0; i < COMMERCIAL_COUNT; i++)
            {
                int index = random.Next(0, _pathways.Count);
                byte[] byteImage = File.ReadAllBytes(_pathways.Keys.ElementAt(index));
                Image image = new Image
                {
                    Data = byteImage,
                    Extension = _pathways.Values.ElementAt(index).TrimStart('.')
                };

                Commerc commercial = new Commerc
                {
                    Id = i + 1,
                    Image = image
                };

                resultCollection.Add(commercial);
            }

            return resultCollection.ToArray();
        }
    }
}
