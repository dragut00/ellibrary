﻿using Ellibrary.Service.Commercial.Config;
using Ellibrary.Service.Commercial.Entities;
using Ellibrary.Service.Commercial.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Ellibrary.Service.Commercial.Realization
{
    public class CommercialRepository : ICommercialRepository
    {
        private IDictionary<string, string> _pathways;
        private const int COMMERCIAL_COUNT = 3;
        public CommercialRepository()
        {
            _pathways = new Dictionary<string, string>();
            var pathwaysSection = ((StartupCommercialConfigSection)ConfigurationManager.GetSection("commercialSettings")).Pathways;
            foreach (CommercialPathElement item in pathwaysSection)
            {
                DirectoryInfo directory = new DirectoryInfo(item.Path);
                FileInfo[] files = directory.GetFiles();
                foreach (var fileInfo in files)
                {
                    _pathways.Add(fileInfo.FullName, fileInfo.Extension);
                }
            }
        }

        public Commerc[] GetCommercials()
        {
            var resultCollection = new List<Commerc>();
            Random random = new Random();
            for (int i = 0; i < COMMERCIAL_COUNT; i++)
            {
                int index = random.Next(0, _pathways.Count);
                byte[] image = File.ReadAllBytes(_pathways.Keys.ElementAt(index));
                Commerc commercial = new Commerc
                {
                    ImageData = image,
                    ImageExtension = _pathways.Values.ElementAt(index).TrimStart('.')
                };
                resultCollection.Add(commercial);
            }

            return resultCollection.ToArray();
        }
    }
}