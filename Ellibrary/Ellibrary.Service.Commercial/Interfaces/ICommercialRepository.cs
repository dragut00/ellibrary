﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Ellibrary.Service.Commercial.Entities;

namespace Ellibrary.Service.Commercial.Interfaces
{
    [ServiceContract]
    public interface ICommercialRepository
    {
        [OperationContract]
        Commerc[] GetCommercials();
    }
}
