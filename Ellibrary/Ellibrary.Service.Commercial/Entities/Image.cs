﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Ellibrary.Service.Commercial.Entities
{
    [DataContract]
    public class Image
    {
        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public string Extension { get; set; }
    }
}