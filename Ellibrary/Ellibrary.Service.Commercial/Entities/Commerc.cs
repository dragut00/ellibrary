﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Ellibrary.Service.Commercial.Entities
{
    [DataContract]
    public class Commerc
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Image Image { get; set; }
    }
}