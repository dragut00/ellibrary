﻿using Ellibrary.App.General.Logger.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Ellibrary.App.Web.Controllers
{
    public class ErrorController : Controller
    {
        private ILoggers _logger;
        public ErrorController(ILoggers logger)
        {
            if (logger == null)
            {
                throw new NullReferenceException(nameof(logger));
            }
            _logger = logger;
        }

        public ActionResult Index(string errorMessage)
        {
            _logger?.ErrorFormat(errorMessage);
            return View("Error");
        }

        public ActionResult BadRequest()
        {
            Response.StatusCode = 400;
            return View("BadRequest");
        }

        public ActionResult PageNotFound()
        {
            Response.StatusCode = 404;
            return View("NotFoundPage");
        }

        public ActionResult ServerNotResponding()
        {
            Response.StatusCode = 500;
            return View("ServerNotRespondingPage");
        }
    }
}
