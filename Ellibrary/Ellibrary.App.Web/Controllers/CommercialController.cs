﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellibrary.App.Web.Models;
using Ellibrary.Service.Commercial;

namespace Ellibrary.App.Web.Controllers
{
    public class CommercialController : Controller
    {
        private ICommercialRepository commercialRepository;

        public CommercialController(ICommercialRepository commercialRepository)
        {
            this.commercialRepository = commercialRepository ?? throw new NullReferenceException();
        }

        public PartialViewResult GetCommercial()
        {           

            var commercials = commercialRepository.GetCommercials();
            var model = new CommercialModel
            {
                Commercials = commercials
            };

            return PartialView("CommercialPartial", model);
        }

        public PartialViewResult GetCommercialVertical()
        {

            var commercials = commercialRepository.GetCommercials();
            var model = new CommercialModel
            {
                Commercials = commercials
            };

            return PartialView("VerticalCommercialPartial", model);
        }


    }
}
