﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellibrary.App.Business.Interfaces;
using Ellibrary.App.Web.Models;

namespace Ellibrary.App.Web.Controllers
{
    public class NavigationController : Controller
    {
        private ICategoryRepository _repository;

        public NavigationController(ICategoryRepository repository)
        {
            if (repository == null)
            {
                throw new NullReferenceException();
            }
            _repository = repository;
        }



        public PartialViewResult Nav()
        {
            NavigationModel model = new NavigationModel{ Categories = _repository.GetCategories()};
            return PartialView(model);
        }

        public PartialViewResult Getsubcategories(int categoryID)
        {
            NavigationModel model = new NavigationModel { Categories = _repository.GetSubcategories(categoryID) };
            return PartialView("SubcategoryPartial", model);
        }

        public PartialViewResult MainNav()
        {
            return PartialView();
        }

    }
}
