﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellibrary.App.Business.Interfaces;
using Ellibrary.App.Web.Models;
using Ellibrary.App.General.Logger;
using Ellibrary.App.General.Logger.Interfaces;
using Ellibrary.App.Data;

namespace Ellibrary.App.Web.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository _repository;
        private ILoggers _logger;        

        
        public BookController(IBookRepository repository, ILoggers logger)
        {
            if (repository == null||logger==null)
            {
                throw new NullReferenceException();
            }
            _repository = repository;
            _logger=logger;
        }
        
        public ActionResult Main()
        {
           BookModel model = new BookModel{Books = _repository.Items.ToList() };
           return View(model);
        }

        public ActionResult GetBooksByCategory(int categoryID)
        {
            BookModel model = new BookModel { Books = _repository.GetBooksByCategory(categoryID) };

            _logger.InfoFormat("Elements by categoryID: " + categoryID, null);

            return View("Main", model);
        }



        public ActionResult GetBookByID(int bookID)
        {
            var model = _repository.GetBookByID(bookID);
            
            _logger.InfoFormat("Get ad by adID: "+bookID, null);

            return View("BookInfo", model);
        }        
    }
}
