﻿using Ellibrary.App.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellibrary.App.Web.Models
{
    public class BookModel
    {
        public ICollection<Book> Books { get; set; }
    }
}