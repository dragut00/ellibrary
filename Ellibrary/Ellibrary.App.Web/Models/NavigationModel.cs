﻿using Ellibrary.App.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellibrary.App.Web.Models
{
    public class NavigationModel
    {
        public ICollection<Category> Categories { get; set; }
    }
}