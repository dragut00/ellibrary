﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellibrary.App.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            if (exception != null)
            {
                Server.ClearError();
                Response.Redirect($"~/Error/Index/?errorMessage={ exception.Message.Replace('\n', ' ') }");
            }
        }
    }
}