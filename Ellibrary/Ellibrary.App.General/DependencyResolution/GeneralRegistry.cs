﻿using Ellibrary.App.General.Logger.Interfaces;
using Ellibrary.App.General.Logger.Realisation;
using log4net.Core;
using StructureMap.Configuration.DSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.General.DependencyResolution
{
    class GeneralRegistry : Registry
    {
        public GeneralRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
            For<ILoggers>().Use(Loggers.GetInstance());
        }
    }
}
