﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.General.Logger.Interfaces
{
   public interface ILoggers
    {
        void DebugFormat(string message, params object[] values);

        void InfoFormat(string message, params object[] values);

        void WarnFormat(string message, params object[] values);

        void ErrorFormat(string message, params object[] values);

        void FatalFormat(string message, params object[] values);
    }
}
