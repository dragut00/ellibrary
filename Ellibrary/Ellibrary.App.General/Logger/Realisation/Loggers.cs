﻿using Ellibrary.App.General.Logger.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellibrary.App.General.Logger.Realisation
{
    public class Loggers:ILoggers
    {
        private static Loggers _logger;
        private static object _object = new object();
        private ILog _log;
        private const string LOGGER_CONFIG = "logger.config";
        private const string LOGGER_NAME = "LOGGER";

        private Loggers()
        {
            _log = LogManager.GetLogger(LOGGER_NAME);
            InitLogger();
        }

        public static Loggers GetInstance()
        {
            lock (_object)
            {
                if (_logger == null)
                {
                    _logger = new Loggers();
                }
            }
            return _logger;
        }

        private void InitLogger()
        { 
            var appConfigPath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.PrivateBinPath, LOGGER_CONFIG);
            var configFile = new FileInfo(appConfigPath);
            XmlConfigurator.ConfigureAndWatch(configFile);
        }

        public void DebugFormat(string message, params object[] values)
        {
            _log.DebugFormat(message, values);
        }

        public void ErrorFormat(string message, params object[] values)
        {
            _log.ErrorFormat(message, values);
        }

        public void FatalFormat(string message, params object[] values)
        {
            _log.FatalFormat(message, values);
        }

        public void InfoFormat(string message, params object[] values)
        {
            _log.InfoFormat(message, values);
        }

        public void WarnFormat(string message, params object[] values)
        {
            _log.WarnFormat(message, values);
        }
    }
}
